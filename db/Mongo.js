var mongodb = require('mongodb')
var MongoClient = mongodb.MongoClient;
var fs = require('fs')
var config = JSON.parse(fs.readFileSync('./static/config.json'))

export default class Mongo {
  constructor () {
    var url = 'mongodb://' + config.mongo + ':27017/MBLTube'
    MongoClient.connect(url.toString(), (err, db) => {
      console.log('[MONGO]:: Connect to Mongo.')

      if(err) {
        console.log('[MONGO]:: Unable to connect to Mongo.')
      } else {
        this.db = db
        var coll = db.collection('PlayList')
        this.coll = coll
      }
    }) //MongoClient
  }

  deleteOne (id, callback) {
    this.coll.deleteOne({ _id : mongodb.ObjectId(id) }, (err, results) => {
      if(err) {
        console.log('[MONGO]:: Unable to delete ' + JSON.stringify(filter) + '.')
      } else {
        if(results['result']['n'] == 1) {
          console.log('[MONGO]:: Delete a music.')
          callback(true)
        } else {
          console.log('[MONGO]:: This music has been deleted.')
          callback(false)
        }
      }
    })
  }

  find(callback) {
    this.coll.find({}).sort({timestamp:1}).toArray( (err, docs) => {
      callback(docs)
    })
  }

  findOne(name, owner, callback) {
    this.coll.findOne({ name: name, owner: owner}, (err, doc) => {
      if(doc == null) {
        callback(false)
      } else {
        callback(true)
      }
    })
  }

  insertOne (music, callback) {
    this.coll.insertOne(music, (err, result) => {
      if(err) {
        console.log('[MONGO]:: Unable to add a music to Mongo.')
        callback(false)
      } else {
        console.log('[MONGO]:: Add a music to Mongo.')
        callback(true)
      }
    })
  }

  remove () {
    this.coll.remove({}, (err, result) => {
      if(!err) {
        console.log('[MONGO]:: Clear the database.')
      }
    })
  }
}
