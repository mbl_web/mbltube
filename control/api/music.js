var express = require('express');
var router = express.Router();
var request = require("request");
var cheerio = require("cheerio");
var fs = require('fs')
var config = JSON.parse(fs.readFileSync('./configure/config.json'))
var socket = require('socket.io-client')('http://localhost:3000')

var assert = require('assert');
import Mongo from '../../db/Mongo'
var MBLTube = new Mongo()

var ipNameMap = {
  '140.113.208.144': 'WestBird',
  '140.113.208.140': 'Daemon',
  '140.113.208.145': 'MoMoCow',
  '140.113.208.134': 'TingXin',
  '140.113.208.133': 'WanChi',
  '140.113.208.132': 'Lego',
  '140.113.208.146': 'Lakey',
  '140.113.208.141': 'GCDeng',
  '140.113.208.142': 'Allen',
  '140.113.208.136': '元昊',
  '140.113.208.137': '智囊'
}

router.post('/order', function(req, res, next) {
  var ip = req.ip.replace('::ffff:', '')
  console.log('[MUSIC]:: Receive an order request from %s.', ip)
  var pattern = /https:\/\/www\.youtube\.com\/watch\?v=\S*/g
  if(!pattern.test(req.body.url)) {
    res.sendStatus(400)
    console.log('[MUSIC]:: User input is noncorrect.')
    next()
  }
  request({
    url: req.body.url,
    method: "GET"
  }, function(e,r,html) {
    if(!e) {
      var $ = cheerio.load(html)
      if(ip in ipNameMap) {
        var owner = ipNameMap[ip]
      } else {
        var owner = 'Guest'
      }
      var music = {
        name: null,
        link: req.body.url,
        duration: null,
        img: null,
        owner: owner,
        ip: ip,
        timestamp: Date.now(),
        cut: 0
      }
      music['name'] = $('span.watch-title').attr('title')

      MBLTube.findOne(music['name'], owner, (result) => {
        if(!result) {
          request({
            url: 'https://www.youtube.com/results?search_query=' + encodeURIComponent(music['name']),
            method: "GET"
          }, function(e,r,html) {
            if(!e) {
              var $ = cheerio.load(html)
              music['duration'] = $('span.video-time').first().text();
              music['img'] = $('.yt-thumb-simple img').first().attr('src')

              MBLTube.insertOne(music, (result) => {
                if(result) {
                  res.sendStatus(200)
                  MBLTube.find( (docs) => {
                    socket.emit('update', docs)
                  })
                  socket.emit('insert')
                  next()
                } else {
                  res.sendStatus(500)
                  next()
                }
              }) // MBLTube.insertOne()
            }
          })
        } else {
          res.sendStatus(202)
          next()
        }
      }) // MBLTube.findOne()

    }
  })
});

router.get('/update', function(req, res, next) {
  MBLTube.find( (docs) => {
    socket.emit('update', docs)
    res.sendStatus(200)
  })
})


module.exports = router;
