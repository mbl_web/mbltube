import React, { Component } from 'react';
import classes from './Header.css'
import 'whatwg-fetch'

export default class Header extends Component {
  constructor() {
    super()
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit(e) {
    e.preventDefault();
    fetch('/api/music/order', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        url: this.input.value,
      })
    }).then(function(res) {
      if(res.status == 200) {
        // alert('A music has been add to the PlayList successfully.')
      } else if(res.status == 202){
        // alert('Your music has existed in the PlayList.')
      } else if(res.status == 400) {
        // alert('Music not found from your URL.')
      } else if(res.status == 500) {
        // alert('Server failed to add your music.')
      }
    })

    this.input.value = ''
  }

  render() {
    return (
      <header className={`${classes.headerWrap}`}>
        <nav className={`container navbar navbar-light`}>
          <div className={`row`}>
            <div className={`col-md-3`}>
              <div className={`container-fluid`}>
                <div className={`col-md-6`} style={{padding: '0'}}>
                  <a
                    className={`navbar-brand ${classes.mbl}`}
                    href="">MBL
                    <span className={`${classes.tube}`}>Tube</span>
                  </a>
                </div>

                <div className={`col-md-6 ${classes.youtube}`} style={{padding: '0'}}>
                  <a className={`navbar-brand`} href="https://www.youtube.com/"></a>
                </div>

              </div>
            </div>

            <div className={`col-md-9`}>
              <form className={`form-inline ${classes.formgroup}`} onSubmit={this.handleSubmit}>
                <input
                  id="input"
                  className={`form-control ${classes.input}`}
                  type="text"
                  placeholder="Youtube URL"
                  ref={(input) => this.input = input}
                />
              <button className={`btn btn-outline-success ${classes.Add}`} type="submit">ADD</button>
              </form>
            </div>
          </div>
        </nav>
      </header>
    );
  }
}
