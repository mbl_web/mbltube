import React, { Component } from 'react';
import classes from './Banner.css'

export default class Banner extends Component {

  render() {
    return (
      <div className={`${classes.banner}`} >
        <h1>No Music, No Life.</h1>
      </div>
    );
  }
}
