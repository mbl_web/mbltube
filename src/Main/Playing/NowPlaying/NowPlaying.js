import React, { Component } from 'react';
import classes from './NowPlaying.css'
import {Song, Sequencer, Sampler} from 'react-music'

export default class NowPlaying extends Component {
  constructor () {
    super()
    this.handleClick = this.handleClick.bind(this)
    this.state = {
      likeActive: false,
      cutActive: false
    }
  }

  componentDidMount () {
    this.props.socket.on('vol', (vol) => {
      this.setState({
        vol: vol
      })
    })
    this.props.socket.emit('vol', 'get')
  }



  handleClick (type) {
    if(type === 'like') {
      this.setState({
        likeActive: !this.state.likeActive
      })
    } else if(type === 'cut') {
      this.props.socket.emit('cut', {
        status: true,
        id: this.props.playing['_id'],
        link: this.props.playing['link']
      })
    } else if(type === 'vUp') {
      this.props.socket.emit('vol', 'up')
    } else if(type === 'vDown') {
      this.props.socket.emit('vol', 'down')
    }
  }

  render () {
    var likeActive = this.state.likeActive? classes.active : classes.none
    var cutActive = this.state.cutActive? classes.active : classes.none

    return (
      <div className={`container-fluid ${classes.playingWrap}`}>
        <div className={`col-md-3 ${classes.musicInfo}`}>
          <img src={this.props.playing['img']} />
          <div className={`${classes.context}`}>
            <div className={`${classes.item}`}>
              <span className={`${classes.owner}`}>
                <i className="material-icons">schedule</i>
                <p>{this.props.playing['duration']}</p>
              </span>
            </div>

            <div className={`${classes.item}`}>
              <span className={`${classes.owner}`}>
                <i className="material-icons">fingerprint</i>
                <p>{this.props.playing['owner']}</p>
              </span>
            </div>

            <div className={`${classes.item}`}>
              <span className={`container-fluid ${classes.volume}`}>
                <i
                  className={`material-icons ${classes.vDown}`}
                  onClick={ () => this.handleClick('vDown')}
                >volume_down</i>
                <div className={`col-md-5 ${classes.vBarWrap}`}>
                  <div
                    className={`${classes.vBar}`}
                    style={{width: this.state.vol + '%'}}
                  ></div>
                </div>
                <i
                  className={`material-icons ${classes.vUp}`}
                  onClick={ () => this.handleClick('vUp')}
                >volume_up</i>
              </span>
            </div>

            <div className={`${classes.item}`}>
              <span
                className={`${classes.like} ${likeActive}`}
                onClick={ () => this.handleClick('like')}
              >
                <i className="material-icons">favorite</i>
                <p>Like</p>
              </span>
              <span
                className={`${classes.cut} ${cutActive}`}
                onClick={ () => this.handleClick('cut')}
              >
                <i className="material-icons">content_cut</i>
                <p>CUT</p>
              </span>
            </div>

          </div>

        </div>
        <div className={`col-md-9`}>
          <h4>
            {this.props.playing['name']}
          </h4>
        </div>
      </div>
    );
  }
}
