import React, { Component } from 'react';
import classes from './Playing.css'
import Banner from './Banner'
import NowPlaying from './NowPlaying'

export default class Playing extends Component {

  render() {
    return (
      <div className={`row`}>
        <div className={`col-md-12`}>
          <div className={`${classes.PlayingWrap}`}>
            {
            (this.props.playing)? <NowPlaying playing={this.props.playing} socket={this.props.socket} />
            : <Banner />
            }
          </div>
        </div>
      </div>
    );
  }
}
