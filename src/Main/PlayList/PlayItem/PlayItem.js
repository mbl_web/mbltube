import React, { Component } from 'react';
import classes from './PlayItem.css'

export default class PlayItem extends Component {
  constructor () {
    super()
    this.handleClick = this.handleClick.bind(this)
    this.state = {
      likeActive: false,
      cutActive: false
    }
  }

  handleClick (index) {
    if(index == 1) {
      this.setState({
        likeActive: !this.state.likeActive
      })
    } else {
      this.props.socket.emit('cut', {
        status: false,
        id: this.props.music['_id'],
        link: this.props.music['link']
      })
    }
  }

  render() {
    var likeActive = this.state.likeActive? classes.active : classes.none
    var cutActive = this.state.cutActive? classes.active : classes.none

    return (
      <div className={`${classes.itemWrap}`}>

          <div className={`${classes.img}`}>
            <img src={this.props.music['img']}></img>
          </div>

          <div className={` ${classes.infos}`}>
            <div className={`${classes.info}`}>
              <span className={`${classes.name}`}>
                <p>{this.props.music['name']}</p>
              </span>
            </div>

            <div className={`${classes.info}`}>
              <span className={`${classes.owner}`}>
                <i className="material-icons">schedule</i>
                <p>{this.props.music['duration']}</p>
              </span>
            </div>

            <div className={`${classes.info}`}>
              <span className={`${classes.owner}`}>
                <i className="material-icons">fingerprint</i>
                <p>{this.props.music['owner']}</p>
              </span>
            </div>

            <div className={`${classes.info}`}>
              <span className={`${classes.like} ${likeActive}`} onClick={ () => this.handleClick(1)}>
                <i className="material-icons">favorite</i>
                <p>Like</p>
              </span>
              <span className={`${classes.cut} ${cutActive}`} onClick={ () => this.handleClick(2)}>
                <i className="material-icons">content_cut</i>
                <p>CUT</p>
              </span>
            </div>

          </div>

      </div>
    );
  }
}
