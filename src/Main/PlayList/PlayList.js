import React, { Component } from 'react';
import PlayItem from './PlayItem'
import classes from './PlayList.css'
import 'whatwg-fetch'

export default class PlayList extends Component {

  render() {
    return (
      <div className={`row`}>
        <div className={`offset-md-3 col-md-9 ${classes.listWrap}`}>
          {
            this.props.playList.length > 0
            ? this.props.playList.map((music, index) =>
                <PlayItem
                  key={index}
                  music={music}
                  socket={this.props.socket}
                />
              )
            : null
        }
        </div>
      </div>
    );
  }
}
