import React, { Component } from 'react';
import classes from './Main.css'
import PlayList from './PlayList'
import Playing from './Playing'
import 'whatwg-fetch'

export default class Main extends Component {

  constructor () {
    super()
    this.state = {
      playList: [],
      socket: null
    }
  }

  componentDidMount () {
    var that = this
    var socket = io.connect();
    that.setState({
      socket: socket
    })
    socket.on('update', (data) => {
      that.setState({
        playList: data
      })
    });
    fetch('/api/music/update')
  }

  render() {
    var playLength = this.state.playList.length
    return (
      <div className={`container ${classes.mainWrap}`}>
        <Playing
          playing={this.state.playList[0]}
          socket={this.state.socket}
        />
        <PlayList
          playList={this.state.playList.slice(1, playLength)}
          socket={this.state.socket}
        />
      </div>
    );
  }
}
