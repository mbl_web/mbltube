import React, { Component } from 'react';
import Header from './Header'
import Main from './Main'
import classes from './App.css'

export default class App extends Component {
  render() {
    return (
      <div className={`${classes.body}`}>
        <Header />
        <Main />
      </div>
    );
  }
}
