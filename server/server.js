import path from 'path'
import express from 'express'
import bodyParser from 'body-parser'
import open from 'open'
import cp from 'child_process'
import loudness from 'loudness'

var config = require('../webpack.config');
var webpack = require('webpack')
var app = express();
var server = require('http').Server(app);
var compiler = webpack(config);
var io = require('socket.io')(server)
var music = require('../control/api/music')

import Mongo from '../db/Mongo'
var MBLTube = new Mongo()
cp.spawn('babel-node', [path.join(__dirname, 'MusicPlayer.js')], { stdio: 'inherit' })
cp.spawn('chromix-too-server')

app.use(require('webpack-dev-middleware')(compiler, {
  publicPath: config.output.publicPath,
  noInfo: true,
  quiet: false
}));
app.use(require('webpack-hot-middleware')(compiler));
app.use( bodyParser.json() );
app.use(express.static('static'));
// user defined
app.use('/api/music', music)

app.get('/', function(req, res) {
  console.log('[SERVER]:: Receive a GET request.')
  res.sendFile(path.join(__dirname, '../index.html'));
});

io.on('connection', function (socket) {
  socket.on('update', function (data) {
    socket.broadcast.emit('update', data);
    console.log('[SERVER]:: Notify clients to update.')
  })
  socket.on('insert', function () {
    socket.broadcast.emit('play');
    console.log('[SERVER]:: Call the music player.')
  })
  socket.on('timeout', function (data) {
    MBLTube.deleteOne(data['id'], (result) => {
      if(result) {
        io.sockets.emit('cut', data['link']);
        console.log('[SERVER]:: Notify the music player timeout.')
      }
      MBLTube.find( (docs) => {
        io.sockets.emit('update', docs)
      })
    })
  })

  socket.on('cut', (data) => {
    MBLTube.deleteOne(data['id'], () => {
      if(data['status']) {
        io.sockets.emit('cut', data['link']);
        console.log('[SERVER]:: Cut the music player.')
      }
      MBLTube.find( (docs) => {
        io.sockets.emit('update', docs)
      })
    })
  })

  socket.on('idle', () => {
    console.log('[SERVER]:: Player is idle now.')
    MBLTube.find( (docs) => {
      if(docs.length) {
        io.sockets.emit('play');
      }
    })
  })

  socket.on('vol', (type) => {
    loudness.getVolume(function (err, vol) {
      if(!err) {
        if(type === 'up') {
          vol = vol + 5
        } else if (type === 'down') {
          vol = vol - 5
        }
        if(vol > 100) {
          vol = 100
        } else if(vol < 0) {
          vol = 0
        }
        console.log('[SERVER]:: Volume is %d now', vol)
        loudness.setVolume(vol, () => {})
        io.sockets.emit('vol', vol);
      }
    })
  })
})

server.listen(3000, function(err) {
  if (err) {
    return console.error(err);
  }

  loudness.setVolume(40, function (err) {
    if(!err) {
      console.log('[SERVER]:: Volume is 40 now')
    }
  });

  console.log('[SERVER]:: Listening at http://localhost:3000/');
})
