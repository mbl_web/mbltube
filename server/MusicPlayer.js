import open from 'open'
import Mongo from '../db/Mongo'
import CP from 'child_process'
import ps from 'ps-node'
var fs = require('fs')
var config = JSON.parse(fs.readFileSync('./configure/config.json'))
var socket = require('socket.io-client')('http://localhost:3000');
var MBLTube = new Mongo()
var isPlaying = false

console.log('[PLAYER]:: Start.')

socket.on('play', function () {
  if (!isPlaying) {
    isPlaying = true
    MBLTube.find( (docs) => {
      if(docs.length) {
        console.log('[PLAYER]:: Find a music and ready to play.')
        var nowPlaying = docs[0]
        open(nowPlaying['link'])

        var timeout = timeTransfer(nowPlaying['duration'])
        setTimeout(function(){
          console.log('[PLAYER]:: A music ending.')
          socket.emit('timeout', { id: nowPlaying['_id'], link: nowPlaying['link']})
        }, timeout*1000);
      } else {
        console.log('[PLAYER]:: PlayList is null.')
      }
    })
  }
})

// socket.on('cut', function () {
//   ps.lookup({
//     command: '/opt/google/chrome/chrome',
//     psargs: 'aux'
//   }, function(err, results) {
//     if (err) {
//       throw new Error( err );
//     }
//     results.forEach(function(process) {
//       ps.kill(process.pid)
//     })
//   })
//   isPlaying = false
//   socket.emit('idle')
// })

socket.on('cut', function (link) {
  CP.spawn('chromix-too', ['rm', link])
  isPlaying = false
  socket.emit('idle')
  console.log('[PLAYER]:: Player is idle now.')
})

var timeTransfer = function(duration) {
  var result = 0;
  var tmp = duration.split(':')
  for(var t in tmp) {
    result *= 60;
    result += parseInt(tmp[t])
  }
  return result < 360 ? result:360
}
