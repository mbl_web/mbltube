var path = require('path');
var webpack = require('webpack');
var express = require('express');
var config = require('./webpack.config');
var bodyParser = require('body-parser');
var music = require('./control/api/music')
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server)
var compiler = webpack(config);
var open = require("open");
var spawn = require('child_process').spawn

app.use(require('webpack-dev-middleware')(compiler, {
  publicPath: config.output.publicPath,
  noInfo: true,
  quiet: false
}));
app.use(require('webpack-hot-middleware')(compiler));
app.use( bodyParser.json() );
// user defined
app.use('/api/music', music)

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.post('*', function (req, res, next) {
  var back = open("http://www.google.com");
  console.log('A post ' + back)
})

io.on('connection', function (socket) {
  // socket.emit('news', { hello: 'world' });
  socket.on('update', function (data) {
    socket.broadcast.emit('update', data);
    console.log('server update')
  })
});

server.listen(3000, function(err) {
  if (err) {
    return console.error(err);
  }

  console.log('Listening at http://localhost:3000/');
})
